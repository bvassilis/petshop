# petstore-front

## Setup

Run `npm install` and `bower install` to download dependencies and Bower packages.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.



# petstore-back

Run `mvn clean package` for building and running the unit tests.