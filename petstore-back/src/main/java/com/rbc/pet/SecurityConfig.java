package com.rbc.pet;

import java.util.Base64;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.google.common.collect.Lists;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		.authorizeRequests()
			.antMatchers(HttpMethod.GET, "/api/**").hasRole(Roles.USER.name())
			.antMatchers(HttpMethod.POST, "/api/**").hasRole(Roles.MANAGER.name())
			.antMatchers(HttpMethod.OPTIONS, "/api/**").hasRole(Roles.USER.name())
			.antMatchers(HttpMethod.DELETE, "/api/**").hasRole(Roles.MANAGER.name())
			.anyRequest().permitAll()
			.and().httpBasic()
			.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
				// enable in memory based authentication with a user named
				// "userA" and "userB"
				.inMemoryAuthentication()
				.withUser(Users.USER_A.getUsername()).password(Users.USER_A.getPassword()).roles(Roles.USER.name())
				.and()
				.withUser(Users.USER_B.getUsername()).password(Users.USER_B.getPassword()).roles(Roles.all());
	}

	public enum Roles {
		USER, MANAGER;

		public static String[] all() {
			return Lists.newArrayList(USER.name(), MANAGER.name()).toArray(new String[0]);
		}
	}

	public enum Users {
		USER_A("userA", "pass1"), USER_B("userB", "pass2");

		private String username;
		private String password;

		Users(String username, String password) {
			this.username = username;
			this.password = password;
		}

		public String getUsername() {
			return username;
		}

		public String getPassword() {
			return password;
		}

		public String getAuthorization() {
			String authorization = username + ":" + password;
			return "Basic " + Base64.getEncoder().encodeToString(authorization.getBytes());
		}

	}

}
