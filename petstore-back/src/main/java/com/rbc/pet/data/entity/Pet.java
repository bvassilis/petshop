package com.rbc.pet.data.entity;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
public class Pet {

	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne
	private Category category;
	@NotNull
	private String name;
    @ElementCollection
	private List<String> photoUrls = Lists.newArrayList();
	@ManyToMany
	private List<Tag> tags = Lists.newArrayList();
	private Status status;

	public static enum Status {
		available, pending, sold;
	}

}
