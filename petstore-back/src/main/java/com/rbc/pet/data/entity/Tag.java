package com.rbc.pet.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
public class Tag {

	@Id
	@GeneratedValue
	private Long id;
	private String name;

}
