package com.rbc.pet.data.repository;

import org.springframework.data.repository.CrudRepository;

import com.rbc.pet.data.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

	Category findByName(String name);
}
