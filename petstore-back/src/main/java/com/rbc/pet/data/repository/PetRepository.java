package com.rbc.pet.data.repository;

import org.springframework.data.repository.CrudRepository;

import com.rbc.pet.data.entity.Pet;

public interface PetRepository extends CrudRepository<Pet, Long> {
}
