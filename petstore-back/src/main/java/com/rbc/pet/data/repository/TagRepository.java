package com.rbc.pet.data.repository;

import org.springframework.data.repository.CrudRepository;

import com.rbc.pet.data.entity.Tag;

public interface TagRepository extends CrudRepository<Tag, Long> {

	Tag findByName(String name);
}
