package com.rbc.pet.data.service;

import java.util.List;

import javax.naming.directory.InvalidAttributesException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.rbc.pet.data.entity.Category;
import com.rbc.pet.data.entity.Pet;
import com.rbc.pet.data.entity.Tag;
import com.rbc.pet.data.repository.CategoryRepository;
import com.rbc.pet.data.repository.PetRepository;
import com.rbc.pet.data.repository.TagRepository;


@Service
public class PetService {

	@Autowired
	private PetRepository petRepository;

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Transactional
	public void insert(Pet pet) throws InvalidAttributesException {
		Category categoryIn = pet.getCategory();
		Category categoryDb = null;
		if(categoryIn != null){
			categoryDb = categoryRepository.findByName(categoryIn.getName());

			if(categoryDb == null) {
				categoryDb = new Category();
				categoryDb.setName(categoryIn.getName());
				categoryRepository.save(categoryDb);
			}
		}

		List<Tag> tagsDb = Lists.newArrayList();
		for(Tag tagIn : pet.getTags()) {
			Tag tagDb = tagRepository.findByName(tagIn.getName());
			if(tagDb == null) {
				tagDb = new Tag();
				tagDb.setName(tagIn.getName());
				tagRepository.save(tagDb);
			}
			tagsDb.add(tagDb);
		}

		pet.setId(null);
		pet.setCategory(categoryDb);
		pet.setTags(tagsDb);

		petRepository.save(pet);
	}



}
