package com.rbc.pet.web.controller;

import java.io.IOException;

import javax.naming.directory.InvalidAttributeIdentifierException;
import javax.naming.directory.InvalidAttributesException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rbc.pet.data.entity.Pet;
import com.rbc.pet.data.repository.PetRepository;
import com.rbc.pet.data.service.PetService;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * Pet Store Service component exposing Pet Store Operations as a rest API / jSON.
 * The service has to be implemented in java language using spring boot.
 *
 * @author vassilis
 *
 */
@RestController
@RequestMapping("/api/v1/pet")
@Slf4j
public class PetstoreController {

	public static long MAX_PET_ID = 1000l;

	@Autowired
	private PetService petService;

	@Autowired
	private PetRepository petRepository;

	/**
	 * Add a new pet to the store
	 *
	 * @param pet incoming json Object
	 * @param bindingResult, validate incoming pet
	 * @return HTTP Status Code
	 * @throws InvalidAttributesException
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public Pet create(@Valid @RequestBody Pet pet, BindingResult bindingResult)
			throws InvalidAttributesException {
		log.trace(pet.toString());
		if (bindingResult.hasErrors()) {
			throw new InvalidAttributesException("Invalid input");
		}
		petService.insert(pet);
		return pet;
	}

	/**
	 *
	 * Find pet by petId
	 *
	 * @param petId
	 * @return Pet Object to Json format on body
	 * @throws InvalidAttributeIdentifierException
	 */
	@RequestMapping(value = "/{petId}", method = RequestMethod.GET)
	@ResponseBody
	public Pet findById(@PathVariable Long petId) throws InvalidAttributeIdentifierException {
		log.trace("Request pet Id: " + petId);
		Pet pet = validateAndReturn(petId);
		return pet;
	}

	/**
	 *
	 * Deletes a pet by petId
	 *
	 * @param petId
	 * @param apiKey
	 * @return
	 * @throws InvalidAttributeIdentifierException
	 */
	@RequestMapping(value = "/{petId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Void> deleteById(@PathVariable Long petId, @RequestHeader(value = "api_key") String apiKey) throws InvalidAttributeIdentifierException {
		log.trace("Request pet Id: " + petId);
		validateAndReturn(petId);
		petRepository.delete(petId);
		return ResponseEntity.ok().build();
	}

	/**
	 * login method
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Void> login() {
		return ResponseEntity.ok().build();
	}

	@ExceptionHandler(InvalidAttributesException.class)
	public void handleInvalidAttributeIdentifierException(HttpServletResponse response) throws IOException {
	    response.sendError(HttpStatus.METHOD_NOT_ALLOWED.value(), "Invalid input");
	}

	@ExceptionHandler(InvalidAttributeIdentifierException.class)
	public void handleInvalidAttributesException(HttpServletResponse response) throws IOException {
	    response.sendError(HttpStatus.BAD_REQUEST.value(), "Invalid ID supplied");
	}

	@ExceptionHandler(EmptyResultDataAccessException.class)
	public void handleEntityNotFoundException(HttpServletResponse response) throws IOException {
	    response.sendError(HttpStatus.NOT_FOUND.value(), "Pet not found");
	}


	private Pet validateAndReturn(Long petId) throws InvalidAttributeIdentifierException {
		if (petId > MAX_PET_ID) {
			throw new InvalidAttributeIdentifierException();
		}
		Pet pet = petRepository.findOne(petId);
		if (pet == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return pet;
	}

}
