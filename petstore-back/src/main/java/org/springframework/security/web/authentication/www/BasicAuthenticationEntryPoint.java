package org.springframework.security.web.authentication.www;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.util.Assert;

import lombok.Getter;
import lombok.Setter;

/**
 * Override class and remove default WWW-Authenticate header, which triggers browsers popup login
 */
@Getter
@Setter
public class BasicAuthenticationEntryPoint implements AuthenticationEntryPoint,
		InitializingBean {

	private String realmName;


	public void afterPropertiesSet() throws Exception {
		Assert.hasText(realmName, "realmName must be specified");
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.web.AuthenticationEntryPoint#commence(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
	 * remove default WWW-Authenticate header
	 */
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
				authException.getMessage());
	}

}
