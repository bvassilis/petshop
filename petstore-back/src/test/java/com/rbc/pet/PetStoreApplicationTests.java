package com.rbc.pet;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PetStoreApplication.class, AppConfig.class, SecurityConfig.class})
@WebAppConfiguration
public abstract class PetStoreApplicationTests {

	@Autowired
    private WebApplicationContext wac;

    protected MockMvc mockMvc;

    protected static ObjectMapper mapper;

    @BeforeClass
    public static void createObjectMapper() {
    	mapper = new ObjectMapper();
    }

    @Before
    public void initWebContextMvc(){
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

}
