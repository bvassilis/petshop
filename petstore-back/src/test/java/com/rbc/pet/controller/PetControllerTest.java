package com.rbc.pet.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.google.common.collect.Lists;
import com.rbc.pet.PetStoreApplicationTests;
import com.rbc.pet.SecurityConfig.Users;
import com.rbc.pet.data.PetTest;
import com.rbc.pet.data.entity.Pet;
import com.rbc.pet.data.repository.CategoryRepository;
import com.rbc.pet.data.repository.PetRepository;
import com.rbc.pet.data.repository.TagRepository;
import com.rbc.pet.web.controller.PetstoreController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PetControllerTest extends PetStoreApplicationTests {

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private PetRepository petRepository;


	///////////////////////////////////////////////////////////////////
	/*
	 *
	 * CREATE PET TESTS
	 *
	 */
	@Test
	public void createTest() throws Exception {

		long expectedTags = tagRepository.count() + 2;
		long expectedCategories = categoryRepository.count() + 1;
		long expectedPets = petRepository.count() + 1;

		Pet pet = createPet(null);

		Assert.assertEquals(expectedTags, tagRepository.count());
		Assert.assertEquals(expectedCategories, categoryRepository.count());
		Assert.assertEquals(expectedPets, petRepository.count());

		// resend the request with the same data
		// no new tags and category should be created in DB,
		// however a new pet DB record will be inserted
		expectedPets = petRepository.count() + 1;

		createPet(pet);

		// tags remains same
		Assert.assertEquals(expectedTags, tagRepository.count());
		//categories remains same
		Assert.assertEquals(expectedCategories, categoryRepository.count());
		// new pet added
		Assert.assertEquals(expectedPets, petRepository.count());
	}

	@Test
	public void createError405Test() throws Exception {
		Pet generatedPet = PetTest.generate();
		// Break validation constraints
		generatedPet.setName(null);
		String petJson = mapper.writeValueAsString(generatedPet);
		log.info(petJson);

		mockMvc.perform(post("/api/v1/pet")
			.contentType(MediaType.APPLICATION_JSON)
			.header("authorization", Users.USER_A.getAuthorization())
			.content(petJson))
			.andExpect(status().is(HttpStatus.METHOD_NOT_ALLOWED.value()))
			.andExpect(handler().handlerType(PetstoreController.class))
			.andReturn();
	}

	///////////////////////////////////////////////////////////////////
	/*
	 *
	 * FIND PET TESTS
	 *
	 */
	@Test
	public void findTest() throws Exception {
		//create DB Record Pet
		createPet(null);
		Assert.assertTrue(petRepository.count() > 0l);
		Pet petDb = petRepository.findAll().iterator().next();

		MvcResult result = mockMvc.perform(get("/api/v1/pet/" + petDb.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", Users.USER_A.getAuthorization()))
			.andExpect(status().isOk())
			.andExpect(handler().handlerType(PetstoreController.class))
			.andReturn();

		String body = result.getResponse().getContentAsString();

		log.debug("BODY: " + body);

		List<String> jsonWords = Lists.newArrayList("id", "category", "name", "photoUrls", "tags", "status", "category", "tag");
		for(String jsonWord : jsonWords) {
			Assert.assertTrue(body.contains(jsonWord));
		}

	}

	///////////////////////////////////////////////////////////////////
	/*
	 *
	 * DELETE PET TESTS
	 *
	 */
	@Test
	public void deleteTest() throws Exception {
		//create DB Record Pet
		createPet(null);

		Assert.assertTrue(petRepository.count() > 0l);
		Iterable<Pet> petsDB = petRepository.findAll();

		//delete all pets from DB
		for(Pet pet : petsDB) {
			mockMvc.perform(delete("/api/v1/pet/" + pet.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", Users.USER_A.getAuthorization())
				.header("api_key", RandomStringUtils.randomAlphanumeric(5)))
				.andExpect(status().isOk())
				.andExpect(handler().handlerType(PetstoreController.class))
				.andReturn();
		}
		Assert.assertEquals(0l, petRepository.count());
	}

	@Test
	public void deleteError400Test() throws Exception {
		long invalidPetId = PetstoreController.MAX_PET_ID + 1;

		mockMvc.perform(delete("/api/v1/pet/" + invalidPetId)
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", Users.USER_A.getAuthorization())
				.header("api_key", RandomStringUtils.randomAlphanumeric(5)))
			.andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
			.andExpect(handler().handlerType(PetstoreController.class))
			.andReturn();

	}

	@Test
	public void deleteError405Test() throws Exception {
		long noPetId = Long.valueOf(RandomStringUtils.randomNumeric(3));

		mockMvc.perform(delete("/api/v1/pet/" + noPetId)
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", Users.USER_A.getAuthorization())
				.header("api_key", RandomStringUtils.randomAlphanumeric(5)))
			.andExpect(status().is(HttpStatus.NOT_FOUND.value()))
			.andExpect(handler().handlerType(PetstoreController.class))
			.andReturn();

	}


	@Test
	public void loginTest() throws Exception {
		mockMvc.perform(get("/api/v1/pet/")
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", Users.USER_A.getAuthorization()))
			.andExpect(status().isOk())
			.andExpect(handler().handlerType(PetstoreController.class))
			.andReturn();

	}


	private Pet createPet(Pet generatedPet) throws Exception {
		if(generatedPet == null) {
			generatedPet = PetTest.generate();
		}
		String petJson = mapper.writeValueAsString(generatedPet);
		log.info(petJson);

		mockMvc.perform(post("/api/v1/pet")
			.contentType(MediaType.APPLICATION_JSON)
			.header("authorization", Users.USER_A.getAuthorization())
			.content(petJson))
			.andExpect(status().isOk())
			.andExpect(handler().handlerType(PetstoreController.class))
			.andReturn();

		return generatedPet;
	}

}
