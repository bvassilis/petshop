package com.rbc.pet.data;

import org.apache.commons.lang3.RandomStringUtils;

import com.rbc.pet.data.entity.Category;

public class CategoryTest {

	public static Category generate() {
		Category category = new Category();
		category.setId(0l);
		category.setName(RandomStringUtils.randomAlphanumeric(5));
		return category;
	}
}
