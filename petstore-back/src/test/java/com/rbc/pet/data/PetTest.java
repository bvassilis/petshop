package com.rbc.pet.data;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.collect.Lists;
import com.rbc.pet.data.entity.Pet;
import com.rbc.pet.data.entity.Pet.Status;

public class PetTest {

	public static Pet generate() {
		Pet pet = new Pet();
		pet.setName(RandomStringUtils.randomAlphanumeric(5));
		pet.setCategory(CategoryTest.generate());
		pet.setId(0l);
		pet.setPhotoUrls(Lists.newArrayList(RandomStringUtils.randomAlphanumeric(5), RandomStringUtils.randomAlphanumeric(5)));
		pet.setStatus(Status.available);
		pet.setTags(Lists.newArrayList(TagTest.generate(), TagTest.generate()));
		return pet;
	}
}
