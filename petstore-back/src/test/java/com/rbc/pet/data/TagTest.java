package com.rbc.pet.data;

import org.apache.commons.lang3.RandomStringUtils;

import com.rbc.pet.data.entity.Tag;

public class TagTest {

	public static Tag generate() {
		Tag tag = new Tag();
		tag.setId(0l);
		tag.setName(RandomStringUtils.randomAlphanumeric(5));
		return tag;
	}
}
