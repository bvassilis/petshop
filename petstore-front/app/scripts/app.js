'use strict';

/**
 * @ngdoc overview
 * @name petStoreHtmlApp
 * @description
 * # petStoreHtmlApp
 *
 * Main module of the application.
 */
angular
  .module('petStoreHtmlApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/pets', {
        templateUrl: 'views/find.html',
        controller: 'FindCtrl',
        controllerAs: 'findCtrl'
      })
      .when('/pets/:petId', {
        templateUrl: 'views/find.html',
        controller: 'FindCtrl',
        controllerAs: 'findCtrl'
      })
      .when('/delete', {
        templateUrl: 'views/delete.html',
        controller: 'DeleteCtrl',
        controllerAs: 'deleteCtrl'
      })
      .when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl',
        controllerAs: 'createCtrl'
      })
      .when('/logout', {
        resolve: {
              logout: ['User', 'Message', function (User, Message) {
                  User.logout();
                  Message.error(200, 'Successfully logout');
              }]
            },
        redirectTo: '/'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
