'use strict';

/**
 * @ngdoc function
 * @name petStoreHtmlApp.controller:CreateCtrl
 * @description
 * # CreateCtrl
 * Controller of the petStoreHtmlApp
 */
 angular.module('petStoreHtmlApp')
 .controller('CreateCtrl', ['$location', 'Pet', 'User', 'Message', function ($location, Pet, User, Message) {
    // Redirect if a user is not logged in
    User.authenticate();

    var i = 0;
    var dataObj = {};

    this.message = Message;

  	// keep tags
  	this.tagsIn = [];

  	// keep photos
  	this.photosIn = [];

  	this.addTag = function() {
  		i++;
  		dataObj = {counter: i};
     this.tagsIn.push(dataObj);
   };

   this.addPhoto = function() {
     i++;
     dataObj = {counter: i};
     this.photosIn.push(dataObj);
   };

   this.createPet = function(petObj) {
    Pet.create(petObj, User.basicAuth()).then(function(response) {
      Message.ok('Pet created!');
      $location.path('/pets/' + response.data.id);
    }, function(response) {
      Message.error(response.data.status, response.data.message);
    });
  };

}]);
