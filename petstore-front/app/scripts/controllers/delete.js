'use strict';

/**
 * @ngdoc function
 * @name petStoreHtmlApp.controller:DeleteCtrl
 * @description
 * # DeleteCtrl
 * Controller of the petStoreHtmlApp
 */
 angular.module('petStoreHtmlApp')
 .controller('DeleteCtrl', ['User', 'Pet', 'Message', function (User, Pet, Message) {
    // Redirect if a user is not logged in
    User.authenticate();

    this.petform = {
    	id: 0,
    	apiKey: ''
    };

    this.deleteById = function() {
      Pet.delete(this.petform.id, this.petform.apiKey, User.basicAuth()).then(function() {
        Message.ok('Pet deleted!');
      }, function(response) {
        Message.error(response.data.status, response.data.message);
      });
    };


}]);
