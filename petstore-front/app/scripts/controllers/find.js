'use strict';

/**
 * @ngdoc function
 * @name petStoreHtmlApp.controller:FindCtrl
 * @description
 * # FindCtrl
 * Controller of the petStoreHtmlApp
 */
 angular.module('petStoreHtmlApp')
 .controller('FindCtrl', ['$location', '$routeParams', 'Pet', 'User', 'Message', function ($location, $routeParams, Pet, User, Message) {

  	// Redirect if a user is not logged in
  	User.authenticate();

    this.message = Message;

    this.petId = parseInt($routeParams.petId || 0);
    this.pet = Pet;

    if($routeParams.petId) {
      Pet.find($routeParams.petId, User.basicAuth()).then(function(response) {
        Message.ok('Pet found!');
        Pet.save(response.data);
      }, function(response) {
        Message.error(response.data.status, response.data.message);
        Pet.reset();
      });
    }

    this.redirectById = function() {
      $location.path('/pets/' + this.petId);
    };


  }]);
