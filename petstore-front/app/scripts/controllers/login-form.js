'use strict';

/**
 * @ngdoc function
 * @name petStoreHtmlApp.controller:LoginFormCtrl
 * @description login page
 * # LoginFormCtrl
 * Controller of the petStoreHtmlApp
 */
 angular.module('petStoreHtmlApp')
 .controller('LoginformCtrl', ['$scope', '$location', 'User', 'Message', function($scope, $location, User, Message) {
 	this.user = {
		username: '',
		password: ''
 	};

 	this.message = Message;

 	this.authorize = function() {
 		var user = this.user;
 		User.login(this.user).then(function() {
 			User.save(user);
 			Message.ok('login');
 			$location.path('#/pets');
 		}, function(response) {
 			Message.error(response.data.status, response.data.message);
 		});
 	};

 }]);
