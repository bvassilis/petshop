'use strict';

/**
 * @ngdoc function
 * @name petStoreHtmlApp.controller:LoginCtrl
 * @description login page
 * # LoginCtrl
 * Controller of the petStoreHtmlApp
 */
angular.module('petStoreHtmlApp')
  .controller('LoginCtrl', ['User', function (User) {

  	// Redirect if a user is not logged in
  	User.authenticate('/pets');
  }]);
