'use strict';

/**
 * @ngdoc function
 * @name petStoreHtmlApp.controller:navLinks
 * @description
 * # navLinks
 * Directive navigation links of the petStoreHtmlApp
 */
 angular.module('petStoreHtmlApp')
 .directive('navLinks', ['User', 'Message', 'Pet', function (User, Message, Pet) {
  return {
   restrict: 'A',
   templateUrl: 'views/nav-links.html',
   controller: function() {
    this.user = User.get();
    this.active = 2;
    this.selectLink = function(setLink){
      this.active = setLink;
      Message.reset();
      Pet.reset();
    };
    this.isSelected = function(checkLink){
      return this.active === checkLink;
    };
    this.reset = function() {
     this.active = 2; 
     Pet.reset();
   };
 },
 controllerAs: 'navLinksCtrl'
};
}]);