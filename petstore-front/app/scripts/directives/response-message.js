'use strict';

/**
 * @ngdoc function
 * @name petStoreHtmlApp.directive:responseMessage
 * @description
 * # responseMessage
 * Directive response message of the petStoreHtmlApp
 */
angular.module('petStoreHtmlApp')
  .directive('responseMessage', ['Message', function (Message) {
    return {
    	restrict: 'E',
    	templateUrl: 'views/response-message.html',
    	controller: function() {
    		this.message = Message;
    	},
    	controllerAs: 'responseMessageCtrl'
    };
  }]);