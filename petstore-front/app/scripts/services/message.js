'use strict';

/*
message data
*/

angular.module('petStoreHtmlApp')
.factory('Message', function MessageFactory() {
	var message = {
		status: 0,
		reason: ''
	};
	

	return {
		ok: function(reason) {
			message.status = 200;
			message.reason = 'Succefully ' + reason;
		},
		error: function(status, reason) {
			message.status = status;
			message.reason = reason;
		},
		get: function() {
			var msg = message.status + ': ' + message.reason;
			return msg;
		},
		reset: function() {
			message.status = 0;
			message.reason = '';
		},
		inactive: function() {
			return message.status === 0;
		},
		isOk: function() {
			return message.status === 200;
		}
	};
});