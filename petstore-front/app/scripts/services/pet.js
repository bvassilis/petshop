'use strict';

/*
Handle pet ajax data calls and keep the finded pet
*/

angular.module('petStoreHtmlApp')
.factory('Pet', ['$routeParams', '$http', function PetFactory($routeParams, $http) {

  var pet = {};


  return {
    find: function(id, basicAuth){
      return $http({method:'GET', url: '/api/v1/pet/' + id,
          headers: {'Authorization': basicAuth}
      });
    },
    delete: function(id, key, basicAuth){
      return $http({method:'DELETE', url: '/api/v1/pet/' + id,
          headers: {
            'Authorization': basicAuth,
            'api_key': key
          }
      });
    },
    create: function(petObj, basicAuth) {
      if(petObj.tags){
        var arrTags = Object.keys(petObj.tags).map(function (key) {return petObj.tags[key];});
        petObj.tags = arrTags;
      }
      if(petObj.photos){
        var arrPhotos = Object.keys(petObj.photos).map(function (key) {return petObj.photos[key].url;});
        petObj.photoUrls = arrPhotos;
      }
      return $http({method: 'POST', url: '/api/v1/pet', data: petObj,
          headers: {'Authorization': basicAuth}
      });
    },
    save: function(petObj) {
      pet = petObj;
    },
    reset: function() {
      pet = {};
    },
    get: function() {
      return pet;
    }
  };
}]);