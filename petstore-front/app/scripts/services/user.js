'use strict';

/*
user login data
*/

angular.module('petStoreHtmlApp')
.factory('User', function UserFactory($http, $location) {
	var user = {
		username: '',
		password: '',
		empty: true
	};
	

	return {
		get: function() {
			return user;
		},
		save: function(newUser) {
			user.username = newUser.username;
			user.password = newUser.password;
			user.empty = false;
		},
	    login: function(userObj) {
	      return $http({
	      	method:'GET', 
	      	url: 'http://localhost/api/v1/pet', 
	      	headers: {'Authorization': this.basicAuth(userObj)}
	      });
	    },
	    basicAuth: function(userObj) {
	    	var userTmp = userObj || user;
	    	return 'Basic ' + btoa(userTmp.username + ':' + userTmp.password);
	    },
	    logout: function() {
	    	user.username = '';
			user.password = '';
			user.empty = true;
	    },
	    authenticate: function(path) {
	    	this.login(user).then(function() {
	 			if(path){
		 			$location.path(path);
	 			}
	 		}, function() {
	 			$location.path('/');
	 		});
	    }
	};
});