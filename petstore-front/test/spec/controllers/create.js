'use strict';

describe('Controller: CreateCtrl', function () {

  // load the controller's module
  beforeEach(module('petStoreHtmlApp'));

  var CreateCtrl,
  scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CreateCtrl = $controller('CreateCtrl', {
      $scope: scope
    });
  }));


  it('should addTag and addPhoto functions increase arrays', function () {
    var expectTagsInLength = CreateCtrl.tagsIn.length + 1;
    CreateCtrl.addTag();
    expect(CreateCtrl.tagsIn.length).toBe(expectTagsInLength);


    var expectPhotosInLength = CreateCtrl.photosIn.length + 1;
    CreateCtrl.addPhoto();
    expect(CreateCtrl.photosIn.length).toBe(expectPhotosInLength);

  });

});
