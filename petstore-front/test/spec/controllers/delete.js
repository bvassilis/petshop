'use strict';

describe('Controller: DeleteCtrl', function () {

  // load the controller's module
  beforeEach(module('petStoreHtmlApp'));

  var DeleteCtrl,
  scope, Message, Pet;

  // Initialize the facroty
  beforeEach(inject(function (_Message_) {
    Message = _Message_;
  }));

  beforeEach(inject(function (_Pet_) {
    Pet = _Pet_;
  }));

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DeleteCtrl = $controller('DeleteCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should fail to delete pet', function () {
    DeleteCtrl.deleteById();
    expect(Message.isOk()).toBe(false);
  });



});
