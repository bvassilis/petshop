'use strict';

describe('Controller: FindCtrl', function () {

  // load the controller's module
  beforeEach(module('petStoreHtmlApp'));

  var FindCtrl,
    location;

   // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $location) {
    location = $location;
    FindCtrl = $controller('FindCtrl', {
      $location: location
      // place here mocked dependencies
    });
  }));

  it('should redirect to pets/pet.id', function () {
  	spyOn(location, 'path');    
    FindCtrl.redirectById();
    expect(location.path).toHaveBeenCalledWith('/pets/0');
  });

  

});
