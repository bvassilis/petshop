'use strict';

describe('Controller: LoginformCtrl', function () {

  // load the controller's module
  beforeEach(module('petStoreHtmlApp'));

  var LoginformCtrl, scope,
    Message, User;

  // Initialize the facroty
  beforeEach(inject(function (_Message_) {
    Message = _Message_;
  }));

  beforeEach(inject(function (_User_) {
    User = _User_;
  }));

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
  	scope = $rootScope.$new();
    LoginformCtrl = $controller('LoginformCtrl', { 
      $scope: scope
    });
  }));

  it('should fail to authorize', function () {
    LoginformCtrl.authorize();
    expect(Message.isOk()).toBe(false);
  });

});
