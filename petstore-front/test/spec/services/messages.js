'use strict';

describe('Service: Message', function () {

  // load the facroty's module
  beforeEach(module('petStoreHtmlApp'));

  var Message;

  // Initialize the facroty
  beforeEach(inject(function (_Message_) {
  	Message = _Message_;
  }));

  it('should ok return 200 status message', function () {
  	Message.ok(1);
  	expect(Message.isOk()).toBe(true);
  	expect(Message.inactive()).toBe(false);
  	expect(Message.get()).toContain('200');
  });

  it('should error return status and reason', function () {
  	var status = 310;
  	var reason = 'Any';
  	Message.error(status, reason);
  	expect(Message.isOk()).toBe(false);
  	expect(Message.inactive()).toBe(false);
  	expect(Message.get()).toContain('310');
  	expect(Message.get()).toContain('Any');
  });

  it('should reset return 0 status and no reason, and to be inactive', function () {
  	Message.reset();
  	expect(Message.isOk()).toBe(false);
  	expect(Message.inactive()).toBe(true);
  	expect(Message.get()).toEqual('0: ');
  });

  
});
