'use strict';

describe('Service: Pet', function () {

  // load the facroty's module
  beforeEach(module('petStoreHtmlApp'));

  var Pet;

  // Initialize the facroty
  beforeEach(inject(function (_Pet_) {
  	Pet = _Pet_;
  }));

  it('should reset pet', function () {
  	var empty = {};
  	Pet.reset();
  	expect(Pet.get()).toEqual(empty);
  });

  it('should save pet', function () {
  	var petObj = {name: 'myname'};
  	Pet.save(petObj);
  	expect(Pet.get()).toEqual(petObj);
  });

 
});
