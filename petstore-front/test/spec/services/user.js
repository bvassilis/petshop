'use strict';

describe('Service: User', function () {

  // load the facroty's module
  beforeEach(module('petStoreHtmlApp'));

  var User;

  // Initialize the facroty
  beforeEach(inject(function (_User_) {
  	User = _User_;
  }));

  it('should save a user', function () {
  	var username = 'user';
  	var password = 'pass';
  	var user = {
		username: username,
		password: password
	};
  	
  	User.save(user);

  	expect(User.get().username).toBe(username);
  	expect(User.get().password).toBe(password);
  	expect(User.get().empty).toBe(false);
  	
  });

 
});
